import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { LoginScreen, SplashScreen } from './screens';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { currentScreen: 'splash' };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setTimePassed();
    }, 3000)
  }

  setTimePassed() {
    this.setState(
      { currentScreen: 'login' }
    );
  }

  render() {
    const { currentScreen } = this.state
    let renderContext = currentScreen === 'splash' ?
      <SplashScreen /> : <LoginScreen />
    return renderContext;
  }
}

