import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { styles } from './style';
import BottomTabNavigator from '../../config/tab-route';

export class FundListScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = { timer: 0 }
    }
    static navigationOptions = {
        title: 'Collections',
    };

    render() {
        return (
            <BottomTabNavigator/>
        )
    }
} 
