import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class CollectionScreen extends React.Component {
    static navigationOptions = {
        title: 'Collections',
    };

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Collection!</Text>
            </View>
        )
    }
} 
