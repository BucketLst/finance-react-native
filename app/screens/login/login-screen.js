import React, { Component } from 'react';
import {
    StyleSheet, Text, View, Image,
    TouchableWithoutFeedback, StatusBar,
    TextInput, SafeAreaView, Keyboard, TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native'
import LoginRoute from '../../config/login-route';
import { styles } from './style';

export class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            isAuthenticated: false
        };
    }

    onLogin = () => {
        this.setState({ isAuthenticated: true });
       // this.props.navigation.navigate('fundList');
        // const { username, password } = this.state;
        // Alert.alert('Credentials', `${username} + ${password}`);
    }

    onForgotPassword = () => {

    }

    displayContext() {
        if (this.state.isAuthenticated) {
            return (<LoginRoute />);
        } else {
            return (
                <KeyboardAvoidingView behavior='padding' style={styles.container}>
                    <TouchableWithoutFeedback style={styles.container}
                        onPress={Keyboard.dismiss}>
                        <View style={styles.container}>
                            <View style={styles.logoContainer}>
                                {/* <Image style={styles.logo}
                                    source={require('../images/logo.png')}>
                                </Image> */}
                                <Text style={styles.title}>L&T Financial Services</Text>
                            </View>
                            <View style={styles.infoContainer}>
                                <TextInput style={styles.input}
                                    placeholder="Mobile Number or Email Id"
                                    placeholderTextColor='grey'
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    autoCorrect={false}
                                    onSubmitEditing={() => this.refs.txtPassword.focus()}
                                />
                                <TextInput style={styles.input}
                                    placeholder="Password"
                                    placeholderTextColor='grey'
                                    returnKeyType='go'
                                    secureTextEntry
                                    autoCorrect={false}
                                    ref={"txtPassword"}
                                />
                                <TouchableOpacity style={styles.buttonContainer}
                                    onPress={this.onLogin}>
                                    <Text style={styles.buttonText}>SIGN IN</Text>
                                </TouchableOpacity>
                                <Text style={styles.forgetPassword} onPress={this.onForgotPassword}>
                                    {"Forgot password?"}
                                </Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>);
        }
    }

    render() {
        return this.displayContext();
    }
}

