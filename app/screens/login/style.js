import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#707070',
    },
    logoContainer: {
        flex: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 128,
        height: 60
    },
    title:{
        color:'white',
        fontSize: 24,
        fontWeight: "bold",
        textAlign: 'center',
        opacity: 0.9,
        fontStyle: "italic"        
    },
    infoContainer: {
        flex: 6,
        paddingLeft: 20,
        paddingRight: 20
    },
    input: {
        backgroundColor: 'white',
        color: 'grey',
        marginBottom: 20,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'        
    },
    buttonContainer: {
        padding: 10,
        backgroundColor: '#0097a7',
        paddingVertical: 10
    },
    buttonText: {
        textAlign: 'center',
        color :'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    forgetPassword: {
        paddingTop: 30,
        fontSize: 18,
        textAlign: 'center',
        color :'white',
    }
});