import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class HelpScreen extends React.Component {
    static navigationOptions = {
        title: 'Help',
    };

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Help!</Text>
            </View>
        )
    }
} 
