import { createStackNavigator } from 'react-navigation';
import { FundListScreen } from '../screens/fund-list/fund-list-screen'

const LoginRoute = createStackNavigator(
    {
      fundList: FundListScreen
    },
    {
      initialRouteName: 'fundList',
    }
  );

export default LoginRoute;