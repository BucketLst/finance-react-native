import { createBottomTabNavigator } from 'react-navigation';
import { HomeScreen } from '../screens/home/home-screen';
import { CollectionScreen } from '../screens/collection/collection-screen';
import { HelpScreen } from '../screens/help/help-screen';

const BottomTabNavigator = createBottomTabNavigator({
  Home: { screen: HomeScreen },
  Collection: { screen: CollectionScreen },
  Help: { screen: HelpScreen }
},
{
  tabBarOptions: {
    labelStyle: {
      fontSize: 14,
    },
    tabStyle: {
      width: 100,
    },
    style: {
      backgroundColor: '#e0e0e0',
    },
  }
});

export default BottomTabNavigator;